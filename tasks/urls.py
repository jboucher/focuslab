from django.conf import settings
from django.urls import path

from tasks import views

app_name = "tasks"

urlpatterns = [
    path("<int:project_id>/<str:project_slug>/", views.project_detail, name="project_detail"),
]
