import datetime

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from rolepermissions.decorators import has_permission_decorator

from tasks.models import Project, Task


@login_required
@has_permission_decorator('view_projects')
def list_projects(request) -> HttpResponse:
    """
    Homepage view - list of projects a user can view, and ability to add a list.
    """

    thedate = datetime.datetime.now()
    # todo: create search form
    # searchform = SearchForm(auto_id=False)

    # Make sure user belongs to at least one group.
    if not request.user.groups.all().exists():
        messages.warning(
            request,
            "You do not yet belong to any workgroups. Ask your administrator to add you to one.",
        )

        # Superusers see all lists
        projects = Project.objects.all().order_by("group__name", "name")
        if not request.user.is_superuser:
            projects = projects.filter(group__in=request.user.groups.all())

        project_count = projects.count()

        # superusers see all lists, so count shouldn't filter by just lists the admin belongs to
        if request.user.is_superuser:
            task_count = Task.objects.filter(completed=0).count()
        else:
            task_count = (
                Task.objects.filter(completed=0)
                    .filter(project__group__in=request.user.groups.all())
                    .count()
            )

        context = {
            "projects": projects,
            "thedate": thedate,
            # "searchform": searchform,
            "project_count": project_count,
            "task_count": task_count,
        }

        return render(request, "tasks/list_lists.html", context)
