import datetime

import bleach
from django.contrib import messages
from django.conf import settings
from tasks.integrations.gitlab import gitlab
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render

# from tasks.forms import AddEditTaskForm
from tasks.models import Task, Project


# from tasks.utils import send_notify_mail


@login_required
def project_detail(request, project_id=None, project_slug=None, view_completed=False) -> HttpResponse:
    """
    Display and manage tasks in a project.
    """

    # Defaults
    vcs_issues = 'N/A'
    vcs_issues_open = 'N/A'
    vcs_issues_closed = 'N/A'
    vcs_percentage = 'N/A'
    overdue_count = 0
    project_percentage = 0
    project = None
    # form = None

    # Which tasks to show on this list view?
    if project_slug == "mine":
        tasks = Task.objects.filter(assigned_to=request.user)

    else:
        # Show a specific project, ensuring permissions.
        project = get_object_or_404(Project, id=project_id)
        if project.group not in request.user.groups.all() and not request.user.is_superuser:
            raise PermissionDenied
        tasks = Task.objects.filter(project=project.id)

    # Additional filtering
    if view_completed:
        tasks = tasks.filter(completed=True)
    else:
        tasks = tasks.filter(completed=False)

    try:
        # Get current number of Active Tasks
        task_count = (
            Task.objects.filter(project_id=project_id)
                .count()
        )

        complete_task_count = (
            Task.objects.filter(completed=1)
                .filter(project__group__in=request.user.groups.all())
                .count()
        )

        active_task_count = (
            Task.objects.filter(completed=0)
                .filter(project_id=project_id)
                .count()
        )

        # Percentage of complete
        task_percent = round(complete_task_count / task_count * 100)

        # Count of overdue tasks
        if request.user.is_superuser:
            overdue_count = Task.objects.filter(due_date__lt=datetime.date.today()).count()
        else:
            overdue_count = (
                Task.objects.filter(due_date__lt=datetime.date.today())
                    .filter(project__group__in=request.user.groups.all())
                    .count()
            )
        if project.gitlab_project_id:
            vcs_issues_open = gitlab.get_issue_count(project.gitlab_project_id, 'opened')
            vcs_issues_closed = gitlab.get_issue_count(project.gitlab_project_id, 'closed')

            vcs_issues = vcs_issues_open + vcs_issues_closed
            vcs_percentage = round(vcs_issues_closed / vcs_issues * 100)

        else:
            vcs_issues = 'N/A'

        if vcs_issues != 'N/A':
            project_percentage = round((vcs_issues_closed + complete_task_count) / (vcs_issues + task_count) * 100)
        else:
            project_percentage = round(complete_task_count / task_count * 100)
    except ZeroDivisionError:
        task_count = 0
        task_percent = 0



    # todo: create Add New Task Form in project detail view
    # ######################
    #  Add New Task Form
    # ######################

    # if request.POST.getlist("add_edit_task"):
    #     form = AddEditTaskForm(
    #         request.user,
    #         request.POST,
    #         initial={"assigned_to": request.user.id, "priority": 999, "project": project},
    #     )
    #
    #     if form.is_valid():
    #         new_task = form.save(commit=False)
    #         new_task.created_by = request.user
    #         new_task.description = bleach.clean(form.cleaned_data["description"], strip=True)
    #         form.save()
    #
    #         # Send email alert only if Notify checkbox is checked AND assignee is not same as the submitter
    #         if (
    #                 "notify" in request.POST
    #                 and new_task.assigned_to
    #                 and new_task.assigned_to != request.user
    #         ):
    #             send_notify_mail(new_task)
    #
    #         messages.success(request, 'New task "{t}" has been added.'.format(t=new_task.title))
    #         return redirect(request.path)
    # else:
    #     # Don't allow adding new tasks on some views
    #     if project_slug not in ["mine", "recent-add", "recent-complete"]:
    #         form = AddEditTaskForm(
    #             request.user,
    #             initial={"assigned_to": request.user.id, "priority": 999, "project": project},
    #         )

    context = {
        "project_id": project_id,
        "project_slug": project_slug,
        "project": project,
        # "form": form,
        "tasks": tasks,
        "task_count": task_count,
        "overdue_count": overdue_count,
        "complete_task_count": complete_task_count,
        "active_task_count": active_task_count,
        "task_percent": task_percent,
        "vcs_issues": vcs_issues,
        "vcs_percentage": vcs_percentage,
        "vcs_issues_open": vcs_issues_open,
        "project_percentage": project_percentage,
        "view_completed": view_completed,
    }

    return render(request, "tasks/project_detail.html", context)
