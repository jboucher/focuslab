from django.contrib import admin
from tasks.models import Project, Task, Comment, Attachment, Board, BoardColumn, BoardTemplate, Workgroup


class TaskAdmin(admin.ModelAdmin):
    list_display = ("title", "project", "completed", "priority", "due_date")
    list_filter = ("project",)
    ordering = ("priority",)
    search_fields = ("title",)


class CommentAdmin(admin.ModelAdmin):
    list_display = ("author", "date", "snippet")


class AttachmentAdmin(admin.ModelAdmin):
    list_display = ("task", "added_by", "timestamp", "file")
    autocomplete_fields = ["added_by", "task"]


class BoardPanelInline(admin.TabularInline):
    model = BoardColumn
    extra = 0


@admin.register(Board)
class BoardAdmin(admin.ModelAdmin):
    list_display = ("name", "project")
    inlines = [BoardPanelInline, ]


# Register your models here.
admin.site.register(Project)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(Attachment, AttachmentAdmin)
admin.site.register(BoardTemplate)
admin.site.register(Workgroup)
