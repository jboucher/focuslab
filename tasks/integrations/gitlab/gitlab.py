import gitlab
from configuration.models import VCS
import logging

logger = logging.getLogger(__name__)

obj = VCS.objects.first()

host = getattr(obj, 'host')
access_token = getattr(obj, 'access_token')


def connect():
    gl = gitlab.Gitlab(host, access_token)
    logger.info("Connected to Gitlab")
    return gl


def get_issue_count(project_id, state):
    count = 0
    gl = connect()
    issue_count = gl.projects.get(project_id, lazy=True).issues.list(state=state)
    for issue in issue_count:
        count += 1
    return count


def get_projects_list():
    gl = connect()
    projects = gl.projects.list(all=True)
    return projects
