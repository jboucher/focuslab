from rolepermissions.permissions import register_object_checker
from focuslab.roles import Manager


@register_object_checker()
def access_project(role, user, project):
    if role == Manager:
        return True

    if user.project == project:
        return True

    return False
