from rolepermissions.roles import AbstractUserRole


class Manager(AbstractUserRole):
    available_permissions = {
        'create_project': True,
        'create_task': True,
        'create_role': True,
        'create_user': True,
        'view_projects': True,
    }


class Staff(AbstractUserRole):
    available_permissions = {
        'view_projects': True,
        'assign_task_complete': True,
    }
