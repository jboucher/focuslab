"""focuslab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include

from frontend import views

handler404 = 'frontend.views.error_404'
handler401 = 'frontend.views.error_401'
handler500 = 'frontend.views.error_500'
handler403 = 'frontend.views.error_403'
handler400 = 'frontend.views.error_400'

urlpatterns = [
    path('', views.Home.as_view(), name="home"),
    path("login", auth_views.LoginView.as_view(), name="login"),
    path("logout", auth_views.LogoutView.as_view(), name="logout"),
    path('admin/', admin.site.urls),
    path('projects/', include('tasks.urls', namespace="tasks")),
    path('avatar/', include('avatar.urls')),
]
