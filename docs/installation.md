title: Installation
description: Installation Instructions
authors: Justin Boucher
date: 2020-04-03

# Installation

## Requirements
This application was written in Django, and contains the following requirements:

* `Python 3.6+` - Python version 3.6 or above.
* `Pip` - Python package installer. Installation instruction for pip found <a href="https://pip.pypa.io/en/stable/installing/" target="_blank">here</a> ![external link](images/external-link-symbol.png).
* `Apache 2.4` with `mod_wsgi` - Apache web server. Apache will need to be installed prior to setup; however, `mod_wsgi` should be installed, configured, and enabled as part of the Quick Start setup script.
* `PostgreSQL 9.5+` - Database server for production use.
* `Git` - Git client to clone application to your local instance. If you would rather install from source see [Installation from source](#quick-start-from-source).

??? question "Can I use something other than Apache?"
    FocusLab uses **WSGI** to provide the content to the web browser. You may use **Nginx**, **Gunicorn**, or any other web framework the supports **WGSI** applications and **Python 3.6+**. However, this guide assumes you will be using Apache and you will have to configure the other web server on your own. A list of **WGSI supported server frameworks** is available <a href="https://wsgi.readthedocs.io/en/latest/servers.html" target="_blank">here</a> ![external link](images/external-link-symbol.png). 
    
    _If you choose to use a different web server framework, it is ==**highly recommended**== that you use the [Installation without setup script](#installation-without-setup-script) method of setup to ensure no errors occur during setup and initial configuration._

??? question "Can I use something other than Postgres?"
    The short answer to this question is: **No**. However, it is possible to reconfigure the internal code in FocusLab to use a different database. This requires *advanced* knowledge of Python and Django and not discussed in this documentation. A starting resources can be found <a href="https://docs.djangoproject.com/en/3.0/ref/databases/" target="_blank">here</a> ![external link](images/external-link-symbol.png).

---
## Quick Start
There are two options for getting started quickly. You can either use a **Git Clone** or by **downloading the source as a .tar.gz**. However, the quick start approach does rely on a few assumptions. If you have problems with the **Quick Start**, try using the [Installation without setup script](#installation-without-setup-script) method of setup.

### Quick Start from Git (Recommended)

``` shell tab="Terminal"
sudo mkdir /opt/focuslab
chown -R $USER /opt/focuslab
cd /opt/focuslab
git clone https://gitlab.com/jboucher/focuslab
pip install -r requirements.txt
python bin/setup.py
```

???+ hint
    Replace `$USER` with the user account that will run FocusLab if you don't have a `$USER` environmental variable set.

??? question "Do I have to use the /opt/focuslab directory?"
    **No**. You can use any directory you wish. The quick start setup script was built to get the system up and running as quick as possible. In order to change the directory you just need to modify the `bin/setup.py` file with the following code:
    ```python
    # GLOBALS
    FOCUSLAB_DIR = "/opt/focuslab" # Replace this directory with your own
    ```
    This code sets an environmental variable so the setup always knows where the FocusLab directory is installed. This environmental value is used in multiple places in the application and must be set for the application to run properly.

### Quick Start from source

``` shell tab="Terminal"
sudo mkdir /opt/focuslab
sudo tar -zxvf focuslab.tar.gz -C /opt/focuslab
chown -R $USER /opt/focuslab
cd /opt/focuslab
pip install -r requirements.txt
python bin/setup.py
```

???+ hint
    Replace `$USER` with the user account that will run FocusLab if you don't have a `$USER` environmental variable set.

??? question "Do I have to use the /opt/focuslab directory?"
    **No**. You can use any directory you wish. The quick start setup script was built to get the system up and running as quick as possible. In order to change the directory you just need to modify the `bin/setup.py` file with the following code:
    ```python
    # GLOBALS
    FOCUSLAB_DIR = "/opt/focuslab" # Replace this directory with your own
    ```
    This code sets an environmental variable so the setup always knows where the FocusLab directory is installed. This environmental value is used in multiple places in the application and must be set for the application to run properly.

## Installation without setup script
Apparently you prefer to be brave, or there was an issue in the `setup.py` script. Here is a terminal version of the steps performed in the `setup.py` script:

```shell tab="Terminal"
# todo: write this script
```