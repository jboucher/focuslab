title: Home
description: Introduction
authors: Justin Boucher
date: 2020-04-02

# Welcome to FocusLab
<img src="images/focus.png" width="300"/> 

For full documentation visit [https://gitlab.com/jboucher/focuslab](https://gitlab.com/jboucher/focuslab).


## Introduction
FocusLab was developed to fix a very systemic problem in project management: Focusing your teams priorities. There are a lot of project management and task management applications out there, but they typically are either single purpose, or don't integrate with your other management apps. Project management apps that can provide all these features are usually out of budget for small teams. FocusLab was created to try to bridge the gap between simple task management and integration.

## Who should use FocusLab
FocusLab is not a replacement for other, more enterprise ready, project management apps. This application is really created for use will small teams that just need a simple way to manage their tasks. 

## Features

|Feature | Description|
|:-------|:-----------|
|**Project and Task Creation** | Managed through Workgroups so you can determine who will have access to a project and its associated items by simply adding the user to a specific Workgroup. |
|**Kanban Boards** | To easily derive the current state or workflow process of tasks and projects.|
|**Version Control System (VCS) Integration** | For linking code issues with tasks. Currently supports the following VCS engines: <ul><li><a href="https://www.gitlab.com" target="_blank">**Gitlab**</a> ![external link](images/external-link-symbol.png)</li><li><a href="https://www.github.com" target="_blank">**Github**</a> ![external link](images/external-link-symbol.png)</li></ul> |
|**File Management** | For file sharing with your team that don't necessarily belong to a task.|
|**Statistical Engine** | Built-in statistics engine so you can make data-driven decisions.|
|**Splunk Integration** | Review logs and metrics within Splunk. _Splunk app for FocusLab is currently in development_. |
|**Audit Logging** | Log user activity and changes they make. Used mostly for security purposes, but has multiple other uses as well. |

