title: Working With Kanban Boards
description: Information about Kanban board system
authors: Justin Boucher
date: 2020-04-02

# Working With Kanban Boards

## What is a Kanban board?

!!! quote "Quoted from Wikipedia"
    Kanban boards visually depict work at various stages of a process using cards to represent work items and columns to represent each stage of the process. Cards are moved from left to right to show progress and to help coordinate teams performing the work. A Kanban board may be divided into horizontal "swimlanes" representing different kinds of work or different teams performing the work.
    
    Kanban boards can be used in knowledge work or for manufacturing processes[^1].
    
    Simple boards have columns for "waiting", "in progress" and "completed" or "to-do", "doing", and "done". Complex Kanban boards can be created that subdivide "in progress" work into multiple columns to visualise the flow of work across a whole value stream map[^2].
    
A much more detailed description can be found <a href="https://www.atlassian.com/agile/kanban/boards" target="_blank">here</a> ![external link](images/external-link-symbol.png).

### Traditional Kanban components

Component Name | Description
:--------------|:-----------
Cards | This is the visual representation of tasks. Each card contains information about the task and its status such as deadline, assignee, description, etc.
Columns | Each column on the board represents a different stage of your workflow. The cards go through the workflow until their full completion.
Work-in-Progress Limits | They restrict the maximum amount of tasks in the different stages of the workflow. Limiting WIP allows you to finish work items faster, by helping your team to focus only on current tasks.
Swimlanes | These are horizontal lanes you can use to separate different types of activities, teams, classes of service, and so on. *Currently, FocusLab __does not__ support Swimlanes.*

---
## Using Kanban in FocusLab

### Creating Kanban boards
When you create a new project you will be asked if you want to create a new board. Boards are not required for creating a project; however, it is recommended that one is created to track the progression of actions on a project. With that said, board creation is opt-out when creating a project. 

???+ Danger "Multiple Boards in a Project"
    **Each project should only have one board!** Multiple boards in a project will cause workflow management problems. This is not a FocusLab recommendation, but a general Kanban workflow recommendation in which FocusLab adheres to.

If you choose to configure a board for the project. You will be given several different options to choose from in the project creation wizard. The table below provides information about each configuration option.

Option | Description | Required
:------|:------------|:--------:
Project| Name of project to assign this board to. This will be the same name used for the Project you create. | Yes
Workgroup| Workgroup that has access to this board. This will be the workgroup that has access to this Project. | Yes
Columns| List of columns to add to the board. There are two options for adding columns: <ol><li>[__Use a template__](#using-pre-defined-templates) - Select from a list of pre-defined templates.</li><li>[__Create you own__](#creating-your-own-columns) - Add your own column names to the board. </li></ol> _See notes below this table for more specific information about each type._  | Yes
Tags | Adding tags to your board gives you the ability to better define the type of board, or to assist in searching for a specific board or board type | No

**Next:** Click the **Add Project** button and the board will be generated with all the information you provided, and you are all set to start managing your workflow processes.

#### Using pre-defined templates
If you choose to use a pre-defined template, you only need to select the template type from the dropdown list, and the columns will be auto-loaded for you. You may edit the column names in the board before saving, if you just need to modify an existing template. Editing column names from a template on a specific board **will not** modify the template. If you need to modify a template see the [Modify existing templates](board_templates.md#modify-an-existing-template) section of this guide.

??? tip "Tip: You can create your own custom templates"
    If you or your organization has a specific template they use, you can create your own custom templates in FocusLab to match your organizational needs. ==You can even mark your custom template as a favorite so it's always on the top of the selection list.== More information on creating your own template can be found in the [Creating custom templates](board_templates.md#creating-custom-templates) section of this guide.

#### Creating your own columns
You can name each column for the board by entering in the name you wish to use in the text box. By default, `Done` is already added to each board and **may not be removed or duplicated**, since it is a reserved column keyword. This reserved keyword controls the automatic completion of tasks. These values are case-sensitive and will appear in the board exactly as written. 

Column names should be provided in a comma separated list. _The order of the values in this element will identify the order the columns appear on the board_.

### Adding a board to an existing project
You may add a board to a project that doesn't currently have a board by going to the **Project Summary** page for the project, and clicking the **Add Board** button from the **Project Actions** panel to the right of the Project Metrics. When the button has been clicked a window will open and you will be able to add the follow information about the board:

Option | Description | Required
:------|:------------|:--------:
Columns| List of columns to add to the board. There are two options for adding columns: <ol><li>[__Use a template__](#using-pre-defined-templates) - Select from a list of pre-defined templates.</li><li>[__Create you own__](#creating-your-own-columns) - Add your own column names to the board. </li></ol> | Yes
Tags | Adding tags to your board gives you the ability to better define the type of board, or to assist in searching for a specific board or board type | No

**Next:** Click the **Add Project** button and the board will be generated with all the information you provided, and you are all set to start managing your workflow processes.


### Editing Kanban boards

#### Adding additional columns to board

#### Changing the order of columns

### Adding tasks to columns

### Moving tasks to new workflow phase

[^1]: J. M. Gross, Kenneth R. McInnis: Kanban Made Simple—Demystifying and Applying Toyota's Legendary Manufacturing Process. Amacom, USA 2003, p. 50. ISBN 0-8144-0763-3. _Source https://en.wikipedia.org/wiki/Kanban_board_
[^2]: "On Setting Your Initial WIP Limits". The Agile Director. 2014-12-07. Retrieved 2015-06-08. _Source https://en.wikipedia.org/wiki/Kanban_board_