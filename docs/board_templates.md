title: Kanban Board Templates
description: Default Column Templates and custom templating
authors: Justin Boucher
date: 2020-04-02

# Kanban Board Templates
This is a list of default template descriptions automatically generated in FocusLab for Kanban style board usage. These templates are pre-canned best practice states to meet multiple project type needs. These templates will also order the columns in the most likely workflow format. In addition, you may also create your own [custom column templates](#creating-custom-templates).

## Pre-configured templates

### Simple
Simple template used for determining new tasks, which ones are currently being worked on, and what has been completed. This is the simplest template available.

Column Title | Description 
:-----------|:-----------
Requested   | All new tasks that aren't actively being worked on. These tasks may be assigned or unassigned to a user.
In Progress | Assigned tasks that are in some stage of development or workflow process.
Done        | Completed tasks.

### Development
Basic development workflow process. If you need to identify expanded testing workflow, consider using the [Development/QA template](#developmentqa-template).

Column Title    | Description 
:--------------|:-----------
Design         | All new tasks that aren't actively being worked on. These tasks may be assigned or unassigned to a user.
Ready to Start | Staging area. Design has been completed, but no development has begun.
Development    | Assigned tasks that are currently in development.
Deployment     | Task being deployed to non-development environment. Ex: `Staging` or `Production`
Verification   | Testing and code verification stage.
Done           | Completed tasks.

### Development/QA
Expanded development workflow process including QA testing statuses.

Column Title    | Description 
:--------------|:-----------
Requested      | Gathering requirements stage. Task is not actively being worked.
Design/Analyze | Task in the design and planning phase.
Development    | Assigned tasks that are currently in development.
Code Review    | Developer review of code and development testing.
Ready For Testing | Developer code testing complete, and staged to be tested by another team (such as QA).
Testing In Progress | QA testing and/or UAT.
Sign-Off | Task ready for deployment
Deployment     | Task being deployed to production environment.
Done           | Completed tasks.

### Level 1 Support
Level 1 or Tier 1 Support. Used for HelpDesk operations or simple customer facing support.

Column Title    | Description 
:--------------|:-----------
Requested      | Gathering requirements stage. Task is not actively being worked.
Working | Task currently in work stage.
Customer Feedback | Awaiting customer feedback.
Pending    | Pending or waiting on external entity.
Confirmation | Awaiting customer sign off.
Done | Task completed.

### Level N Support
Level N Support was created to expand the [Level 1 Support Template](#level-1-support-template) for your Level/Tier 2,3, etc... support or breakfix tasks.

Column Title    | Description 
:--------------|:-----------
Requested      | Gathering requirements stage. Task is not actively being worked.
Analyze | Researching task and determining a mitigation plan.
Fix | Task currently in progress.
Verify | Verifying that mitigation plan works. Testing phase.
Ready  | Testing complete and ready to deploy to production, or ready to send to customer
Done | Task completed.

### Master Process
Default Master Process Workflow. This template allows for basic process workflow from a project that just needs requirements gathering, planning, and execution phases.

Column Title    | Description 
:--------------|:-----------
Requested      | Gathering requirements stage. Task is not actively being worked.
Plan | Research and planning phase.
In Progress | Task currently in progress.
Pending Internal | Task waiting on internal resources.
Pending External | Task waiting on external resources.
Verify Production | Verify that associated items in task work in Production.
Done | Task completed.

### Priority
Simple priority management template, similar to a very basic Epic story.

Column Title    | Description 
:--------------|:-----------
None | No priority identified.
Critical | Task listed as Critical priority.
High | Task listed as High priority.
Medium | Task listed as Medium priority.
Low | Task listed as Low priority.
Done | Task completed.

### Project Planning
General project planning phases.

Column Title    | Description 
:--------------|:-----------
Plan | Research and planning phase.
Breakdown | Breakdown of task elements to assign to staff.
Ready To Start | Staging area. Plan and breakdown has been completed, but no work on task has begun.
In Progress | Task currently in progress.
On Hold | Task is pending on another resource.
Done | Task completed.
Abandoned | Task is no longer needed for this project. These tasks should be closed. **Task will not automatically be closed by FocusLab**, and must either be marked done or moved to Done before moving task to this board. If this is not followed, it will affect the metrics in the dashboard.

### Sysadmin
Sysadmin style process workflow. Allows for System Administrators to track workflows such as patching, installing software, or any process that requires requirements gathering and monitoring.

Column Title    | Description 
:--------------|:-----------
Requested | Gathering requirements stage. Task is not actively being worked.
Working | Task currently in progress.
Pending | Task is pending on another resource.
Monitoring | Items in task are currently being monitored or tested
Done | Task completed.

## Modify an existing template
To modify an existing template, go to __Task Settings-->Board Column Templates__ and select the template you wish to modify. You will be able to modify the following information:

| Element       | Description                                        | Sample                     |
|:------------- |:---------------------------------------------------|:--------------------------|
| Columns        | Each column name for boards. **These values are case-sensitive**. Column names should be provided in a comma separated list. _**You will not be able to remove the Done column** since it is a required panel for all boards_ ==_The order of the values in this element will identify the order the columns appear on the board_.==  |  `New,In Progress,Review` |
| Favorite      | Adds template to top of the template selection dropdown list under the Favorites submenu. Select the checkbox to mark this template as a favorite. | **Default:** `False`


## Creating custom templates
To add custom templates, create a new template under __Task Settings-->Board Column Templates__. Template must contain the following items:

| Element       | Description                                        | Sample                     |
|:------------- |:---------------------------------------------------|:--------------------------|
| Name          | Name of Template used in Sample Templates dropdown when creating new boards. Values are not case-sensitive. | `Foo` or `foo`             |
| Columns        | Each column name for boards. **These values are case-sensitive**. Column names should be provided in a comma separated list. ==_The order of the values in this element will identify the order the columns appear on the board_.==  |  `New,In Progress,Done` |
| Favorite      | Adds template to top of the template selection dropdown list under the Favorites submenu. Select the checkbox to mark this template as a favorite. | **Default:** `False`

???+ attention 
    __Done is a reserved column title and must be listed in every template.__ Preferably at the end as it is usually the final state of a tasks workflow. All completed tasks will automatically be moved to the _Done_ column by default.
    