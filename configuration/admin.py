from django.contrib import admin
from configuration.models import VCS


class VCSAdmin(admin.ModelAdmin):
    list_display = ("type", "host", "access_token")
    list_filter = ("type",)
    ordering = ("type",)


admin.site.register(VCS, VCSAdmin)
