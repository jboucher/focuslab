from django.db import models


class VCS(models.Model):
    type = models.CharField(max_length=20)
    host = models.CharField(max_length=50)
    access_token = models.CharField(max_length=100)

    def __str__(self):
        return self.type

    class Meta:
        ordering = ["type"]
        verbose_name_plural = "VCS"
