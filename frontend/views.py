from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


class Home(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'


def error_400(request, exception):
    data = {}
    return render(request, 'frontend/error-400.html', data)


def error_401(request, exception):
    data = {}
    return render(request, 'frontend/error-401.html', data)


def error_403(request, exception):
    data = {}
    return render(request, 'frontend/error-403.html', data)


def error_404(request, exception):
    data = {}
    return render(request, 'frontend/error-404.html', data)


def error_500(request, exception=None):
    data = {}
    return render(request, 'frontend/error-500.html', data)
